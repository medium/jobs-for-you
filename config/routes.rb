Rails.application.routes.draw do

  root to: 'jobs#index'

  resources :jobs, except: [:show] do
    get :applicants

    collection do
      get :skills_list
    end
  end

  resources :applicants, except: [:show] do
    get :jobs

    collection do
      get :skills_list
    end
  end
end
