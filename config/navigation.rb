SimpleNavigation::Configuration.run do |navigation|
  navigation.selected_class = 'active'

  navigation.items do |primary|
    primary.dom_attributes = { class: 'nav nav-pills nav-stacked' }

    primary.item :jobs, 'Вакансии', root_path,
      highlights_on: /\A\/($|jobs)/

    primary.item :applicants, 'Соискатели', applicants_path,
      highlights_on: /\A\/applicants/
  end
end
