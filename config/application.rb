require File.expand_path('../boot', __FILE__)

require 'rails/all'

Bundler.require(*Rails.groups)

module JobsForYou
  class Application < Rails::Application
    config.time_zone = 'Moscow'
    config.autoload_paths << Rails.root.join('lib')
    config.assets.paths << Rails.root.join('vendor', 'assets', 'bower_components')

    config.i18n.available_locales = [:ru]
    config.i18n.default_locale = :ru

    config.active_record.raise_in_transactional_callbacks = true
  end
end
