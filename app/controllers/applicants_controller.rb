class ApplicantsController < ApplicationController
  include SkillsList

  def index
    @applicants = Applicant.order('desired_salary ASC').all
  end

  def destroy
    applicant = Applicant.where(id: params[:id]).first
    applicant.destroy
    redirect_to action: :index
  end

  def jobs
    @applicant = Applicant.where(id: params[:applicant_id]).first

    if @applicant
      @full_match = @applicant.full_match_jobs.actual.
        order('salary DESC')
      @partially_match = @applicant.partially_match_jobs.
        actual.order('salary DESC')
    else
      status 404
    end
  end

  def new
    @applicant = Applicant.new
  end

  def create
    @applicant = Applicant.new(applicant_params)

    if @applicant.save
      redirect_to action: :index
    else
      render :new
    end
  end

  def edit
    @applicant = Applicant.where(id: params[:id]).first.decorate

    unless @applicant
      render status: 404

    end
  end

  def update
    @applicant = Applicant.where(id: params[:id]).first.decorate

    if @applicant.update_attributes(applicant_params)
      redirect_to action: :index
    else
      render :edit
    end
  end
  
  private

  def applicant_params
    params.require(:applicant).permit(:name, :contact,
      :status, :desired_salary, :skills => [])
  end
end
