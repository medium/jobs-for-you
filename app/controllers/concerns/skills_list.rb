module SkillsList
  extend ActiveSupport::Concern

  included do
    def skills_list
      term = params[:q]

      render json: 
        (Applicant.distinct_skills(term) +
         Job.distinct_skills(term)).
        uniq.map{|skill| { id: skill, text: skill } }
    end
  end
end
