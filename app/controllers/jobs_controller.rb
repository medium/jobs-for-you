require 'pry'

class JobsController < ApplicationController
  include SkillsList

  def index
    @jobs = Job.order('salary ASC').all
  end

  def destroy
    job = Job.where(id: params[:id]).first
    job.destroy
    redirect_to action: :index
  end

  def applicants
    @job = Job.where(id: params[:job_id]).first

    if @job
      @full_match = @job.full_match_applicants.active.
        order('desired_salary ASC')
      @partially_match = @job.partially_match_applicants.
        active.order('desired_salary ASC')
    else
      status 404
    end
  end

  def new
    @job = Job.new
  end

  def create
    @job = Job.new(job_params)

    if @job.save
      redirect_to action: :index
    else
      render :new
    end
  end

  def edit
    @job = Job.where(id: params[:id]).first.decorate

    unless @job
      render status: 404

    end
  end

  def update
    @job = Job.where(id: params[:id]).first.decorate

    if @job.update_attributes(job_params)
      redirect_to action: :index
    else
      render :edit
    end
  end
  
  private

  def job_params
    params.require(:job).permit(:name,
      :issue_date, :valid_time, :salary,
      :contact, :skills => [])
  end
end
