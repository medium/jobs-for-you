class ApplicantDecorator < Draper::Decorator
  delegate_all

  def skills
    object.skills
  end
end
