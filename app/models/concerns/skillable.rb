module Skillable
  extend ActiveSupport::Concern

  included do
    validates :skills, presence: true
  end

  module ClassMethods
    def distinct_skills(term = "")
      ActiveRecord::Base.connection.execute("SELECT DISTINCT unnest(skills) as skill FROM #{table_name}").
        to_a.map{ |o| o['skill'] }.
        find_all{ |o|
          if term.present?
            o.downcase.include?(term.downcase)
          else
            true
          end
        }
    end
  end
end

