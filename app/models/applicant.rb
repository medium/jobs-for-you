require 'pry'

class Applicant < ActiveRecord::Base
  include Skillable

  enum status: [ :active, :inactive ]

  validates :name, :contact, :status, :desired_salary,
    presence: true

  validates :name, format: {
    with: /\A[а-яА-Яё]+\s[а-яА-Яё]+\s[а-яА-Яё]+\z/
  }

  validates :desired_salary, numericality: {
    only_integer: true,
    greater_than_or_equal_to: 0
  }

  validate do
    unless contact =~ AuxRegexp::EMAIL_REGEXP ||
      contact =~ AuxRegexp::PHONE_REGEXP

      errors.add :contact, :email_or_phone
    end
  end

  scope :active, -> { where(status: 0) }
  scope :inactive, -> { where(status: 1) }

  def full_match_jobs
    Job.where('ARRAY[?]::varchar[] @> skills', skills)
  end

  def partially_match_jobs
    Job.where('ARRAY[?]::varchar[] && skills', skills).
      where.not('ARRAY[?]::varchar[] <@ skills', skills)
  end
end
