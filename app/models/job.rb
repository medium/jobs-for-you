class Job < ActiveRecord::Base
  include Skillable

  validates :name, :issue_date, :valid_time, :salary,
    :skills, :contact, presence: true

  validates :issue_date, date: {
    before_or_equal_to: proc { Date.today }
  }

  validates :valid_time, numericality: {
    only_integer: true, greater_than: 0
  }

  validates :salary, numericality: {
    only_integer: true,
    greater_than_or_equal_to: 0
  }

  scope :actual, -> do
    where ':today <= ("issue_date" + "valid_time")',
      today: Date.today
  end

  scope :not_actual, -> do
    where ':today > ("issue_date" + "valid_time")',
      today: Date.today
  end

  def is_actual?
    (Date.today - (issue_date + valid_time)).to_i < 0
  end

  def full_match_applicants
    Applicant.where('ARRAY[?]::varchar[] <@ skills', skills)
  end

  def partially_match_applicants
    Applicant.where('ARRAY[?]::varchar[] && skills', skills).
      where.not('ARRAY[?]::varchar[] <@ skills', skills)
  end
end
