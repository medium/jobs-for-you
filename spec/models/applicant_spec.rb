require 'rails_helper'

RSpec.describe Applicant, type: :model do
  describe '::Validation' do
    subject { build :applicant, options }
    let(:options) { {} }

    it_behaves_like 'valid'

    context 'with latin name' do
      it_behaves_like 'not valid',
        name: "Alexander Brandon Brandonovich"
    end

    context 'with two words name' do
      it_behaves_like 'not valid', name: "Александр Пупкин"
    end

    context 'with wrong contact' do
      it_behaves_like 'not valid', contact: "wrong contact"
    end

    context 'with wrong desired_salary' do
      it_behaves_like 'not valid', desired_salary: -1_000
    end

    context 'with empty skills list' do
      it_behaves_like 'not valid', skills: []
    end
  end

  describe 'Jobs' do
    context 'full match' do
      subject { create :applicant, :match_skills1 }

      before do
        create :job, :match_skills1
        create :job
      end

      it 'should match only one job' do
        expect(subject.full_match_jobs.size).to eq(1)
      end
    end

    context 'partial match' do
      subject { create :applicant, :match_skills1 }

      before do
        create :job, :match_skills2
        create :job
      end

      it 'should match only one job' do
        expect(subject.partially_match_jobs.size).to eq(1)
      end
    end
  end
end
