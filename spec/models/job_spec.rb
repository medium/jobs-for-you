require 'rails_helper'

RSpec.describe Job, type: :model do
  describe '::Validation' do
    subject(:job) { build :job, options }
    let(:options) { {} }

    it_behaves_like 'valid'

    context 'with wrong issue_date' do
      it_behaves_like 'not valid', issue_date: Date.tomorrow
    end

    context 'with wrong valid time' do
      it_behaves_like 'not valid', valid_time: -2
    end

    context 'with wrong salary' do
      it_behaves_like 'not valid', salary: -10_000
    end

    context 'with no contact' do
      it_behaves_like 'not valid', contact: nil
    end

    context 'with no name' do
      it_behaves_like 'not valid', name: nil
    end

    context 'with empty skills list' do
      it_behaves_like 'not valid', skills: []
    end
  end

  describe '::Scopes' do
    describe 'actual jobs' do
      before do
        create :job
        create :job, :not_actual
      end

      it 'should return 1 actual jobs' do
        expect(Job.actual.count).to eq(1)
      end

      it 'should return 1 unactual jobs' do
        expect(Job.not_actual.count).to eq(1)
      end
    end
  end

  describe 'Apllicants' do
    context 'full match' do
      subject { create :job, :match_skills1 }

      before do
        create :applicant, :match_skills1
        create :applicant
      end

      it 'should have only one applicant' do
        expect(subject.full_match_applicants.size).to eq(1)
      end
    end

    context 'partial match' do
      subject { create :job, :match_skills1 }

      before do
        create :applicant, :match_skills2
        create :applicant
      end

      it 'should have only one job' do
        expect(subject.partially_match_applicants.size).to eq(1)
      end
    end
  end
end
