require 'rails_helper'

RSpec.describe Skillable do
  describe '::Skills' do
    before do
      create :job, skills: ["CSS3", "HTML5"]
      create :job, skills: ["CSS3", "Javascript"]
    end

    it 'should list distinct skills' do
      expect(Job.distinct_skills).to contain_exactly("CSS3", "HTML5", "Javascript")
    end

    it 'should filter skills by term' do
      expect(Job.distinct_skills('htm')).to contain_exactly("HTML5")
    end
  end
end
