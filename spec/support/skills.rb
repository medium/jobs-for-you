module SupportSkills
  SKILLS = [
    "HTML5",
    "CSS3",
    "Javascript",
    "Coffeescript",
    "Javascript",
    "jQuery",
    "Ruby on Rails",
    "PostgreSQL",
    "MongoDB",
    "SOLID"
  ].freeze

  module_function

  def match_skills1
    SKILLS[0..3]
  end

  def match_skills2
    SKILLS[1..5]
  end

  def not_match_skills
    Array[SKILLS[6..-1].sample]
  end
end
