RSpec.shared_examples 'valid' do |options|
  let(:options) { options } if options.present?

  it 'should be valid' do
    expect(subject.valid?).to be_truthy
  end
end

RSpec.shared_examples 'not valid' do |options|
  let(:options) { options } if options.present?

  it 'should be not valid' do
    expect(subject.valid?).to be_falsey
  end
end
