FactoryGirl.define do
  factory :applicant do
    name { FFaker::NameRU.with_same_sex do
      "#{FFaker::NameRU.first_name} #{FFaker::NameRU.patronymic} #{FFaker::NameRU.last_name}"
    end }
    contact { FFaker::Internet.email }
    desired_salary { rand(1..5) * 10_000 + 80_000 }
    skills { SupportSkills.not_match_skills }
    status :active

    trait :match_skills1 do
      skills { SupportSkills.match_skills1 }
    end

    trait :match_skills2 do
      skills { SupportSkills.match_skills2 }
    end

    trait :inactive do
      status :inactive
    end
  end
end
