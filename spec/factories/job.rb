FactoryGirl.define do
  factory :job do
    name { FFaker::Lorem.sentence(rand 3..5) }
    salary { rand(1..5) * 10_000 + 80_000 }
    contact { FFaker::Lorem.sentence(rand 3..7) }
    skills { SupportSkills.not_match_skills }
    issue_date {
      today = Date.today
      (today.ago(10.days).to_date..today).to_a.sample.to_date
    }
    valid_time 30

    trait :match_skills1 do
      skills { SupportSkills.match_skills1 }
    end

    trait :match_skills2 do
      skills { SupportSkills.match_skills2 }
    end

    trait :not_actual do
      issue_date {
        today = Date.today
        (today.ago(20.days).to_date..today.ago(11.days).to_date).
          to_a.sample.to_date
      }
      valid_time 10
    end
  end
end
