# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150908042147) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "applicants", force: :cascade do |t|
    t.string   "name",                        null: false
    t.string   "contact",                     null: false
    t.integer  "status",         default: 0,  null: false
    t.integer  "desired_salary",              null: false
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.string   "skills",         default: [],              array: true
  end

  add_index "applicants", ["desired_salary"], name: "index_applicants_on_desired_salary", using: :btree
  add_index "applicants", ["name"], name: "index_applicants_on_name", using: :btree
  add_index "applicants", ["status"], name: "index_applicants_on_status", using: :btree

  create_table "jobs", force: :cascade do |t|
    t.string   "name",                    null: false
    t.date     "issue_date",              null: false
    t.integer  "valid_time",              null: false
    t.integer  "salary",                  null: false
    t.string   "contact",                 null: false
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "skills",     default: [],              array: true
  end

  add_index "jobs", ["issue_date"], name: "index_jobs_on_issue_date", using: :btree
  add_index "jobs", ["name"], name: "index_jobs_on_name", using: :btree
  add_index "jobs", ["salary"], name: "index_jobs_on_salary", using: :btree

  create_table "skills", force: :cascade do |t|
    t.string   "name",       default: "0", null: false
    t.integer  "counter",    default: 0,   null: false
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "skills", ["counter"], name: "index_skills_on_counter", using: :btree
  add_index "skills", ["name"], name: "index_skills_on_name", using: :btree

end
