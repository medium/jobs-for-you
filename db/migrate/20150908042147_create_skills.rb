class CreateSkills < ActiveRecord::Migration
  def change
    create_table :skills do |t|
      t.string :name, default: 0, null: false
      t.integer :counter, default: 0, null: false

      t.timestamps null: false

      t.index :name
      t.index :counter
    end
  end
end
