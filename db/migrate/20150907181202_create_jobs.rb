class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
      t.string :name,        null: false
      t.date :issue_date,    null: false
      t.integer :valid_time, null: false
      t.integer :salary,     null: false
      t.string :contact,     null: false

      t.timestamps null: false

      t.index :name
      t.index :issue_date
      t.index :salary
    end
  end
end
