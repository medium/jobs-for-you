class AddSkillsToJobsAndApplicants < ActiveRecord::Migration
  def change
    add_column :jobs, :skills, :string, array: true, default: []
    add_column :applicants, :skills, :string, array: true, default: []
  end
end
