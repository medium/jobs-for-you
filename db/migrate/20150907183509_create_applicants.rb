class CreateApplicants < ActiveRecord::Migration
  def change
    create_table :applicants do |t|
      t.string :name,            null: false
      t.string :contact,         null: false
      t.integer :status,          null: false, default: 'active'
      t.integer :desired_salary, null: false

      t.timestamps null: false

      t.index :name
      t.index :desired_salary
      t.index :status
    end
  end
end
