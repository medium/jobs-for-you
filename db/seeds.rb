# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#

require "factory_girl_rails"
require "ffaker"
require Rails.root.join('spec/support/skills')

3.times do
  FactoryGirl.create :job, :match_skills1
end

3.times do
  FactoryGirl.create :job, :match_skills2
end

3.times do
  FactoryGirl.create :job, :not_actual
end

3.times do
  FactoryGirl.create :applicant, :match_skills1
end

3.times do
  FactoryGirl.create :applicant, :match_skills2
end

3.times do
  FactoryGirl.create :applicant, :inactive
end
