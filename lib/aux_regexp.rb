module AuxRegexp
  EMAIL_REGEXP = /\A
    [^@\s]+
    @
    ([\p{L}\d-]+\.)+
    [\p{L}\d\-]{2,}
    \z
  /ix.freeze

  PHONE_REGEXP = /
    \A
    (\(?\+\d+\)?)?
    [\d \-.()]{4,20}
    \z
  /x.freeze
end
